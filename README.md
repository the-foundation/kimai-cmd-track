
# kimai Command Track API timesheet tracker
---


Simple Wrapper to track your commands into kimai , helping to count your total machine time spent for e.g. backups , deduplication or *younameit*

## INSTALLATION :

* create token for user
* find your project ID (numeric)
* find your activity id (numeric)
* create /etc/kimaicmd2api.conf with the following variables:

| Var | Content | Note |
|---|---|---|
| $KIURL  | https://my.kima.tld/api/timesheets | API URL |
| $KIUSER | username | string user |
| $KITOKEN | MyUltraHardToCrackToken | string token |
| $KIPROJECT | 1 | number projectid |
| $KIACTION | 2 | number activity |

example:
```
KIUSER=loadcron
KITOKEN=NobodyWillEverGuessThisTokenExceptYourMum
KIURL=https://do.not.click.here/api/timesheets
KIPROJECT=1
KIACTION=8
```

---
---


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/kimai-cmd-track/README.md/logo.jpg" width="480" height="270"/></div></a>
