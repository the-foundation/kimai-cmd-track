#!/bin/bash
## KIMAI COMMAND API TIMESHEET TRACKER SHELL SCRIPT

start=$(date +%s)
failed=0
test -f /etc/kimaicmd2api.conf || failed=1 >&2
if [ "$failed" == 1 ];then echo " GhostBustaz are loO°Ooking for your config , /etc/kimaicmd2api.conf is a ghost " >&2 ;[[ "${EXIT_WITHOUT_CONFIG}" = "true" ]] && exit 666 ;fi
. /etc/kimaicmd2api.conf
if [[  -z $KIUSER || -z  $KIURL || -z $KITOKEN || -z $KIPROJECT || -z $KIACTION ]]; then
  echo "echo  i want my money back , one of my friends KIUSER | KIURL | KITOKEN | KIPROJECT | KIACTION is still missing" >&2
   #exit 1;
#else  echo "CONF OK"
fi


#GET START ( COMMENTING VERBOSE )
START=$(date "+%F %T"|tr -cd '[:alnum:] :-')
## WYSIWID (what you see is what it does)
#echo "exec " "$@"

## main()  " you gotta do what thay told ya"
/bin/bash -c "$(echo "$@")"

## wait for absolution ( or better prevent having identic start/end timestamps)
sleep 1;

## "postcard from paris"
MSG='{
    "begin": "'${START}'",
    "end": "'$(date "+%F %T"|tr -cd '[:alnum:] :-')'",
    "project": '${KIPROJECT}',
    "activity": '${KIACTION}',
    "description": "✓ :: kimai-cmd-track on '$(hostname -f)' ||>→→ '$(echo "$@"|tr -cd '[:alnum:] :\-.;_äöÜÖÄÖ#+*~§$%&/{]()=][{¬½¼³²¹' )' ←←←"
}'

##WHEN THE JOB IS REAL WORK AND TOOK LONGER THAN 2 MINUTES WE LOG IT 
end=$(date +%s) ; 
if [[ "$(($start + 120))" -le "$end" ]] ; then 
	## message in a bottle
	curl -X POST  "${KIURL}"    -H 'Content-Type: application/json'   --header "X-AUTH-USER: ${KIUSER}" --header "X-AUTH-TOKEN: ${KITOKEN}"   -H 'cache-control: no-cache'   -d "$MSG" >&2
fi
